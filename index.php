<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Loading mySQL data via jQuery AJAX</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
 $.ajax({
	url: "ajax-jquery-php-data.php",
	data:"page=1",
	success: function(r) {//success(result,status,xhr)
		$("#result").html(r);
	},
	error: function(e) {//error(xhr,status,error)
		alert("ERROR: " + e.status + " " + e.statusText);
	}
});//end of ajax()
function getpage(p){
	$.ajax({
	type:"GET",
	url: "ajax-jquery-php-data.php",
	data:"page="+p,
	success: function(r) {//success(result,status,xhr)
	$("#loader").hide(); // hiding loader message
	$("#result").html(r);
	},
	error: function(e) {//error(xhr,status,error)
		alert("ERROR: " + e.status + " " + e.statusText);
	}
});//end of ajax()
}//end of getpage()
</script>
</head>
<body>
<div id="result"><div id="loader">Loading...</div></div>
</body>
</html>
